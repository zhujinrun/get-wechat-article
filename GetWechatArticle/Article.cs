﻿
namespace GetWechatArticle
{
    public class Article
    {
        //文章信息
        public int id { get; set; }

        /// <summary>
        /// 发布时间
        /// </summary>
        public string times { get; set; }

        /// <summary>
        /// 发布的内容
        /// </summary>
        public string contents { get; set; }

        /// <summary>
        /// 标题
        /// </summary>
        public string title { get; set; }

        /// <summary>
        /// 内容简要
        /// </summary>
        public string digest { get; set; }

        /// <summary>
        /// 文章链接地址
        /// </summary>
        public string url { get; set; }

        /// <summary>
        /// 0为单独内容；1为普通文章
        /// </summary>
        public int type { get; set; }

        /// <summary>
        /// 0为默认，刚写入文章；1-2为生成错误，3为多次重试还是错误，9为被禁文章，10为已经生成HTML
        /// </summary>
        public int status { get; set; }

        private bool _rtnFlag = true;

        /// <summary>
        /// 状态
        /// </summary>
        public bool rtnFlag
        {
            get { return _rtnFlag; }
            set { _rtnFlag = value; }
        }

        /// <summary>
        /// 额外返回内容
        /// </summary>
        public string rtnStr { get; set; }


    }
}
