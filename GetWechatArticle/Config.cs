﻿
namespace GetWechatArticle
{
    /// <summary>
    /// 公共变量
    /// </summary>
    public class Config
    {
        /// <summary>
        /// 获取文章列表的URL{}里的内容为需要替换的内容
        /// 关于此链接是如何来的，请移步：https://www.52pojie.cn/thread-1646638-1-1.html
        /// </summary>
        public static string articleListUrl = "https://mp.weixin.qq.com/mp/profile_ext?action=getmsg&__biz={biz}&f=json&offset={offset}&count=10&is_ok=1&scene=124&uin={uin}&key={key}&pass_ticket={pass_ticket}&wxtoken=&appmsg_token={appmsg_token}&x5=0&f=json";

        /// <summary>
        /// 获取biz的正则式
        /// </summary>
        public static string bizPat = @"_biz=(\S*?)&";

        /// <summary>
        /// 获取uin的正则式
        /// </summary>
        public static string uinPat = @"uin=(\S*?)&";

        /// <summary>
        /// 获取key的正则式
        /// </summary>
        public static string keyPat = @"key=(\S*?)&";

        /// <summary>
        /// 获取pass_ticket的正则式
        /// </summary>
        public static string pass_ticketPat = @"pass_ticket=(\S*?)&";

        /// <summary>
        /// 获取appmsg_token的正则式
        /// </summary>
        public static string appmsg_tokenPat = @"appmsg_token=(\S*?)&";

        /// <summary>
        /// 获取nickname的正则式
        /// </summary>
        public static string nicknamePat = @"var nickname = ""(\S*?)"";";

        /// <summary>
        /// 年-月-日 时:分:秒 正则式
        /// </summary>
        public static string DateTimePat = @"(\d{2}|\d{4})(?:\-)?([0]{1}\d{1}|[1]{1}[0-2]{1})(?:\-)?([0-2]{1}\d{1}|[3]{1}[0-1]{1})(?:\s)?([0-1]{1}\d{1}|[2]{1}[0-3]{1})(?::)?([0-5]{1}\d{1})(?::)?([0-5]{1}\d{1})";

        /// <summary>
        /// 获取时间的正则式
        /// </summary>
        public static string datetimePat = "\"datetime\":(\\d*?),";

        /// <summary>
        /// 获取列表中直接内容的正则式
        /// </summary>
        public static string comm_msg_infoPat = "\"comm_msg_info\":\\{[\\s\\S]*?\"datetime\":(\\d*?),[\\s\\S]*?\"content\":\"([\\s\\S]*?)\"";

        /// <summary>
        /// 获取列表中文章列表的正则式
        /// </summary>
        public static string app_msg_ext_infoPat = "\\{\"title\":\"([\\s\\S]*?)\",\"digest\":\"([\\s\\S]*?)\"[\\s\\S]*?,\"content_url\":\"([\\s\\S]*?)\",[\\s\\S]*?";

        /// <summary>
        ///  获取列表中直接内容及文章列表的正则式
        /// </summary>
        public static string comm_msg_info_and_app_msg_ext_infoPat = comm_msg_infoPat + "[\\s\\S]*?" + app_msg_ext_infoPat.Substring(1);

        /// <summary>
        /// 获取next_offset的正则式
        /// </summary>
        public static string offsetPat = @"""next_offset"":(\d*?),";

        /// <summary>
        /// 获取列表中文章数量的正则式
        /// </summary>
        public static string msgcountPat = @"""msg_count"":(\d*?),";

        /// <summary>
        /// 判断是否大于0的整数
        /// </summary>
        public static string intPat = @"^[1-9][0-9]*?$";

        /// <summary>
        /// 判断是否为10位数时间戳的正则式
        /// </summary>
        public static string datetimeIntPat = @"^\d{10}$";

        /// <summary>
        /// 获取文章内容中文章发表时间的正则式1
        /// 不同文章有时候时间变量会有所不同，所以有多个正则式
        /// </summary>
        public static string timePat = @",t=""([^""]*?)"";";

        /// <summary>
        /// 获取文章内容中文章发表时间的正则式2
        /// </summary>
        public static string timePat2 = "<span id=\"publish_time\" aria\\-hidden=\"hidden\">([\\S\\s]*?)</span>";

        /// <summary>
        /// 获取文章内容中文章发表时间的正则式3
        /// </summary>
        public static string timePat3 = @",n=""([^""]*?)"",";

        /// <summary>
        /// 获取文章内容中文章发表时间的正则式4
        /// </summary>
        public static string timePat4 = @"\{e\(0,""([^""]*?)"",";

        /// <summary>
        /// 获取文章内容中文章发表时间的正则式5
        /// </summary>
        public static string timePat5 = @": '(\d{10})' \* 1;";

        /// <summary>
        /// 获取文章内容中文章发表时间的正则式6，兜底，找''或""中间的10位数字，存在错误的可能
        /// </summary>
        public static string timePat6 = "(?:'|\")(\\d{10})(?:'|\")";

        /// <summary>
        /// 获取文章被删除原因的正则式1
        /// </summary>
        public static string reasonPat = "del_reason: '(\\s\\S)', ";

        /// <summary>
        /// 获取文章被删除原因的正则式2
        /// </summary>
        public static string reasonPat2 = "<div class=\"weui-msg__title warn\">([\\s\\S]*?)</div>";

        /// <summary>
        /// 获取文章标题的正则式1
        /// </summary>
        public static string titlePat = "<meta property=\"og:title\" content=\"([\\S\\s]*?)\" />";

        /// <summary>
        /// 获取文章标题的正则式2
        /// </summary>
        public static string titlePat2 = "document.getElementById\\('js_image_desc'\\).innerHTML = \"([\\S\\s]*?)\"";

        /// <summary>
        /// 文件名不合法字符
        /// </summary>
        public static string titleRepPat = @"[\f\n\r\t\v\?\\/\*><:|""#&\+\'\^\%]";

    }
}
