﻿using HttpCodeLib;
using System;
using System.IO;
using System.Text.RegularExpressions;

namespace GetWechatArticle
{
    /// <summary>
    /// 公共操作方法
    /// </summary>
    public class OP
    {
        #region 正则

        /// <summary>
        /// 找到第一个匹配项
        /// </summary>
        /// <param name="pat">正则式</param>
        /// <param name="str">查找内容</param>
        /// <returns></returns>
        public static string RegExpFindOne(string pat, string str)
        {
            return RegExpFind(pat, str, 1, 0);
        }

        /// <summary>
        /// 找到指定行列匹配项
        /// </summary>
        /// <param name="pat">正则式</param>
        /// <param name="str">查找内容</param>
        /// <param name="index">第几列</param>
        /// <param name="colindex">第几行</param>
        /// <returns></returns>
        public static string RegExpFind(string pat, string str, int index, int colindex)
        {
            int i = 0;
            Regex r = new Regex(pat);
            MatchCollection mc = r.Matches(str);
            try
            {
                foreach (Match m in mc)
                {
                    if (i == colindex)
                    {
                        if (m.Groups.Count >= index)
                        {
                            return m.Groups[index].Value;
                        }
                        else
                            return "";
                    }
                    i++;
                }
            }
            catch (Exception ex)
            {
            }
            return "";
        }

        /// <summary>
        /// 找到最后一个匹配项
        /// </summary>
        /// <param name="pat">正则式</param>
        /// <param name="str">查找内容</param>
        /// <returns></returns>
        public static string RegExpFindLastOne(string pat, string str)
        {
            Regex r = new Regex(pat);
            MatchCollection mc = r.Matches(str);
            try
            {
                if (mc.Count > 0)
                {
                    return mc[mc.Count - 1].Groups[1].Value;
                }
                else
                {
                    return "";
                }
            }
            catch (Exception ex)
            {
            }
            return "";
        }

        /// <summary>
        /// 判断是否存在
        /// </summary>
        /// <param name="pat">正则式</param>
        /// <param name="str">查找内容</param>
        /// <returns></returns>
        public static bool RegIsMatch(string pat, string str)
        {
            bool Flag = false;

            Regex r = new Regex(pat);
            Flag = r.IsMatch(str);

            return Flag;
        }

        /// <summary>
        /// 找出所有匹配项
        /// </summary>
        /// <param name="pat">正则式</param>
        /// <param name="str">查找内容</param>
        /// <returns></returns>
        public static string[,] RegExpFindArray(string pat, string str)
        {
            Regex r = new Regex(pat);
            MatchCollection mc = r.Matches(str);
            int num = mc.Count;
            string[,] arr = new string[,] { };
            try
            {
                if (num > 0)
                    arr = new string[num, mc[0].Groups.Count - 1];
                int n = 0;
                foreach (Match m in mc)
                {
                    if (m.Groups.Count > 0)
                    {
                        for (int i = 1; i < m.Groups.Count; i++)
                        {
                            arr[n, i - 1] = m.Groups[i].Value;
                        }
                    }
                    n++;
                }
            }
            catch (Exception ex)
            {

            }
            return arr;
        }

        /// <summary>
        /// 获取当前文章列表中最后发布文章的时间
        /// </summary>
        /// <param name="html"></param>
        /// <returns></returns>
        public static string GetListLastDate(string html)
        {
            return RegExpFindOne(Config.datetimePat, html);
        }

        #endregion

        /// <summary>
        /// 获取指定网址的HTML
        /// </summary>
        /// <param name="url"></param>
        /// <returns></returns>
        public static string GetUrlContent(string url)
        {
            try
            {
                string res = string.Empty;
                HttpHelpers helper = new HttpHelpers();
                HttpItems items = new HttpItems();
                HttpResults hr = new HttpResults();
                items.URL = url;
                items.IsSetSecurityProtocolType = true;
                //items.SecProtocolTypeEx = (SecurityProtocolTypeEx)Enum.Parse(typeof(SecurityProtocolTypeEx), "Tls12");
                items.SecProtocolTypeEx = SecurityProtocolTypeEx.Tls;
                hr = helper.GetHtml(items);
                res = hr.Html;
                return res;
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        /// <summary>
        /// 将DateTime时间格式转换为Unix时间戳格式
        /// </summary>
        /// <param name="time">时间</param>
        /// <param name="types">时间戳的长度：1是13位，2是10位</param>
        /// <returns>long</returns>
        public static long ConvertDateTimeToInt(System.DateTime time, int types)
        {
            System.DateTime startTime = TimeZone.CurrentTimeZone.ToLocalTime(new System.DateTime(1970, 1, 1, 0, 0, 0, 0));
            if (types == 1)
                return (time.Ticks - startTime.Ticks) / 10000; //除10000调整为13位
            else
                return (time.Ticks - startTime.Ticks) / 10000000; //除10000000调整为10位
        }

        /// <summary>
        /// 将10位Unix时间戳转换为DateTime类型时间
        /// </summary>
        /// <param name="d">double 型数字,10位</param>
        /// <returns>DateTime</returns>
        public static System.DateTime ConvertIntToDateTime10(double d)
        {
            System.DateTime time = System.DateTime.MinValue;
            System.DateTime startTime = TimeZone.CurrentTimeZone.ToLocalTime(new System.DateTime(1970, 1, 1));
            time = startTime.AddMilliseconds(d * 1000);
            return time;
        }

        /// <summary>
        /// 生成HTML文件
        /// </summary>
        /// <param name="filename">文件名</param>
        /// <param name="folder">目录</param>
        /// <param name="html">内容</param>
        public static void CreateHtml(string filename, string folder, string html)
        {
            try
            {
                string path = System.AppDomain.CurrentDomain.BaseDirectory;
                if (folder.Length > 0)
                    path += @"\" + folder;
                if (!Directory.Exists(path))
                    Directory.CreateDirectory(path);
                FileStream fs = new FileStream(path + @"\" + filename + ".html", FileMode.Create, FileAccess.Write);
                StreamWriter mw = new StreamWriter(fs);
                mw.BaseStream.Seek(0, SeekOrigin.End);
                mw.WriteLine(html);
                mw.Flush();
                mw.Close();
                fs.Close();
                fs.Dispose();
            }
            catch { }
        }

    }
}
